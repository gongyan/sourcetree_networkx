# import networkx as nx
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx


def plot_graph(G, weight_name=None):
    '''
    G: a networkx G
    weight_name: name of the attribute for plotting edge weights (if G is weighted)
    '''

    plt.figure()
    pos = nx.spring_layout(G)
    edges = G.edges()
    weights = None

    if weight_name:
        weights = [int(G[u][v][weight_name]) for u, v in edges]
        labels = nx.get_edge_attributes(G, weight_name)
        nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)
        nx.draw_networkx(G, pos, edges=edges, width=weights);
    else:
        nx.draw_networkx(G, pos, edges=edges);


def question_three():
    G = nx.Graph()
    G.add_edge('A', 'B')
    G.add_edge('A', 'C')
    G.add_edge('B', 'D')
    G.add_edge('C', 'D')
    G.add_edge('D', 'E')
    G.add_edge('D', 'G')
    G.add_edge('E', 'G')
    G.add_edge('G', 'F')
    print('is_connected', nx.is_connected(G))
    print('bfs', nx.bfs_tree(G, 'A'))
    # print(nx.center(G))
    # print(nx.periphery(G))
    # print(nx.eccentricity(G))
    # print(G.nodes(data=True))
    # print(G.edges(data=True))
    # plt.figure()
    # nx.draw_networkx(G)
    # plt.show()
    # maixmum distance between nodes
    dia = nx.diameter(G)

    #
    ra = nx.radius(G)
    print('diameter', dia)
    print('radius', ra)
    # print(nx.minimum_node_cut(G))
    # print(nx.shortest_path_length(G,'A'))

    # distance from node A to others, the largest distance be equal to diameter
    print('periphery', nx.periphery(G))
    # distance from node A to others,the  largest distance be equal to radius
    print('Center', nx.center(G))

    # a node n is the largest distance between n and all other nodes.
    print('eccentricity', nx.eccentricity(G))

    # plot_graph(G)


def question_four():
    graph_4 = nx.Graph()
    graph_4.add_edge('A', 'C')
    graph_4.add_edge('A', 'D')
    graph_4.add_edge('B', 'F')
    graph_4.add_edge('B', 'D')
    graph_4.add_edge('C', 'G')
    graph_4.add_edge('D', 'E')
    graph_4.add_edge('D', 'H')
    graph_4.add_edge('E', 'H')
    graph_4.add_edge('E', 'G')
    graph_4.add_edge('F', 'H')
    graph_4.add_edge('I', 'J')
    graph_4.add_edge('I', 'K')
    graph_4.add_edge('E', 'J')
    # plt.figure()
    # nx.draw_networkx(graph_4)
    # plt.show()
    print('is_connected:', nx.is_connected(graph_4))
    print('connected_components:', sorted(nx.connected_components(graph_4)))
    print('custering', nx.clustering(graph_4, 'I'))
    print('custering', nx.clustering(graph_4, 'J'))
    print('custering', nx.clustering(graph_4, 'K'))


def question_five():
    graph_five_a = nx.DiGraph()
    graph_five_b = nx.DiGraph()
    graph_five_c = nx.DiGraph()

    # create graph a
    graph_five_a.add_edge('A', 'C')
    graph_five_a.add_edge('A', 'B')
    graph_five_a.add_edge('B', 'C')
    graph_five_a.add_edge('C', 'B')
    graph_five_a.add_edge('D', 'C')
    graph_five_a.add_edge('E', 'B')
    # graph_five_a.add_edge('C', 'D')

    # create graph b
    graph_five_b.add_edge('A', 'B')
    graph_five_b.add_edge('A', 'D')
    graph_five_b.add_edge('B', 'C')
    graph_five_b.add_edge('B', 'A')
    graph_five_b.add_edge('C', 'A')
    graph_five_b.add_edge('D', 'E')
    graph_five_b.add_edge('E', 'C')

    # create graph c
    graph_five_c.add_edge('A', 'C')
    graph_five_c.add_edge('A', 'D')
    graph_five_c.add_edge('B', 'A')
    graph_five_c.add_edge('C', 'B')
    print('is_weakly', nx.is_weakly_connected(graph_five_a))
    print('is_strongly', nx.is_strongly_connected(graph_five_a))
    print('minimum edge cut', nx.minimum_edge_cut(graph_five_b))
    # # plt.figure()
    # nx.draw_networkx(graph_five_b)
    # plt.show()


def question_seven():
    graph_seven = nx.Graph()
    graph_seven.add_edge('A', 'F')
    graph_seven.add_edge('A', 'B')
    graph_seven.add_edge('A', 'C')
    graph_seven.add_edge('B', 'C')
    graph_seven.add_edge('B', 'D')
    graph_seven.add_edge('B', 'E')
    graph_seven.add_edge('C', 'E')
    graph_seven.add_edge('C', 'F')
    graph_seven.add_edge('D', 'E')
    graph_seven.add_edge('E', 'F')
    graph_seven.add_edge('E', 'G')
    graph_seven.add_edge('E', 'H')
    graph_seven.add_edge('G', 'H')
    graph_seven.add_edge('G', 'M')
    graph_seven.add_edge('G', 'K')
    graph_seven.add_edge('H', 'M')
    graph_seven.add_edge('H', 'N')
    graph_seven.add_edge('M', 'K')
    graph_seven.add_edge('N', 'K')
    # # plt.figure()
    # nx.draw_networkx(graph_seven)
    # plt.show()
    print('node connectivity', nx.node_connectivity(graph_seven))
    print('edge connectivity', nx.edge_connectivity(graph_seven))


def question_nine():
    graph_nine = nx.DiGraph()
    graph_nine.add_edge('A', 'C')
    graph_nine.add_edge('B', 'A')
    graph_nine.add_edge('C', 'B')
    graph_nine.add_edge('C', 'E')
    graph_nine.add_edge('D', 'B')
    graph_nine.add_edge('D', 'A')
    graph_nine.add_edge('E', 'F')
    graph_nine.add_edge('E', 'H')
    graph_nine.add_edge('F', 'G')
    graph_nine.add_edge('F', 'H')
    graph_nine.add_edge('F', 'K')
    graph_nine.add_edge('G', 'K')
    graph_nine.add_edge('H', 'J')
    graph_nine.add_edge('H', 'K')
    graph_nine.add_edge('K', 'J')
    # plt.figure()
    # nx.draw_networkx(graph_nine)
    # plt.show()
    print('simple path from D to K', len(list(nx.all_simple_paths(graph_nine, 'D', 'K'))))
    print('minimum node cut', nx.minimum_node_cut(graph_nine, 'E', 'K'))
    print('minimum edge cut', nx.minimum_edge_cut(graph_nine, 'E', 'K'))


if __name__ == '__main__':
    # question_three()
    question_four()
    # question_five()
    # question_seven()
    # question_nine()
    nx.read_edgelist()
    pd.read_table()
