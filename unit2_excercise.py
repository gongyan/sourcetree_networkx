import networkx as nx
import matplotlib.pyplot as plt
import operator


def answer_one():
    email_graph = nx.read_edgelist('email_network.txt', data=[('time', int)], nodetype=str,
                                   create_using=nx.MultiDiGraph())
    return email_graph  # Your Answer Here


def answer_two():
    email_graph = nx.read_edgelist('email_network.txt', data=[('time', int)], nodetype=str,
                                   create_using=nx.MultiDiGraph())
    degree_dict = email_graph.degree()
    degree_tuple = [(employee, email) for employee, email in degree_dict]
    print(degree_tuple)

    return  # Your Answer Here


def answer_three():
    email_graph = answer_one()
    strongly = nx.is_strongly_connected(email_graph)
    weakly = nx.is_weakly_connected(email_graph)
    print(strongly, weakly)
    # return strongly, weakly


def answer_four():
    # Your Code Here
    email_graph = answer_one()
    weakly_components = nx.weakly_connected_components(email_graph)
    weakly_components_nodes = str(weakly_components)
    print(len(weakly_components_nodes))
    print(type(weakly_components_nodes))
    # print(weakly_components.nodes(data=True))
    # print(len(weakly_components.nodes()))
    return  # Your Answer Here


def answer_five():
    email_graph = answer_one()
    strongly_components = nx.strongly_connected_components(email_graph)
    # strongly_components_nodes = list(strongly_components)
    len_test = str(filter(max, map(len, strongly_components)))
    print(len_test)

    # return len(strongly_components_nodes)


def answer_six():
    # Your Code Here
    email_graph = answer_one()
    G_sc = max(nx.strongly_connected_component_subgraphs(email_graph), key=len)
    # nx.draw_networkx(G_sc)
    # plt.show()
    return G_sc  # Your Answer Here


def answer_seven():
    # Your Code Here
    email_graph = answer_one()
    avg_distance = nx.average_shortest_path_length(email_graph)
    print(avg_distance)
    return  # Your Answer Here


def answer_eleven():
    # Your Code Here
    sub_graph = answer_six()
    diameter_distance = nx.diameter(sub_graph)
    for node in sub_graph.nodes():
        shortest_node = nx.shortest_path_length(sub_graph, node)
        max_shortest = max(shortest_node.items(), key=operator.itemgetter(1))
        if max_shortest[1] == diameter_distance:
            print(max_shortest)
    print(nx.node_connected_component(sub_graph, '97'))

            # return max_shortest
    return  # Your Answer Here


def answer_thirteen():
    email_graph = answer_six()
    multi_un = email_graph.to_undirected()
    G_un = nx.Graph()
    G_un.add_edges_from(multi_un.edges())

    #     nx.set_edge_attributes(G_un,'time',None)
    #     nx.draw_networkx(G_un)
    #     print(G_un.edges(data=True))

    return G_un  # Your Answer Here


def answer_twelve():
    # Your Code Here
    nx.minimum_edge_cut()

    return  # Your Answer Here

if __name__ == '__main__':
    # answer_two()
    # answer_three()
    # answer_four()
    # answer_five()
    # answer_six()
    # answer_seven()
    answer_eleven()
    # answer_thirteen()
